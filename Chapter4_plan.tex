\chapter{Planning with perception and uncertainty}
\label{chap:plan}
\minitoc

The work of the planner is to take a floor plan along with source and destination locations in the floor plan 
as the input and then continuously produce a trajectory for the MAV to follow
till it reaches the destination. This trajectory needs to be smooth, collision-free 
and dynamically feasible, using a  range-limited RGB-D sensor for local obstacle sensing.
We have broken down this problem into multiple sub-problems which consists of primarily two levels of hierarchy a) Global Planner and b) Local Planner.
Global Planner's task is to generate a topological plan using the information from the floor plan, state estimation and updates from local planner.
Local planner's task is to take a section of the topological plan that is in context to current location and generate trajectories that helps fulfill 
the global plan with the desired qualities like smoothness and obstacle avoidance.


\section{Global Planner}
 \label{section:plan_global}
 
 \begin{figure}[!h]
   \begin{center}
    \includegraphics[width=1\textwidth]		{floor.png}
    \caption{Global planning done on floor plan}
    \label{fig:floor}
  \end{center}		
\end{figure}

For an indoor environment the high level plan should be ideally
a topological plan as shown in figure \ref{fig:floor}. Topological plan are difficult to maintain with uncertainty. To do this
We generate 2 graphs in the pre-processing stage a) Information Graph, b) Search Graph and generate a plan. 
In the Query phase we update Search Graph and re-generate the plan as necessary.

 \begin{figure}[!h]
  \subfloat[Initial floor plan]{\label{figur:1}\includegraphics[width=0.48\linewidth]{nsh_2nd_floor_plan.png}}
  \hspace{0.1cm}
  \subfloat[Marked with entry/exit points]{\label{figur:2} \includegraphics[width=0.48\linewidth]{nsh_2nd_floor_plan_marked.png}}
 \caption{Marking small openings between regions to find separate regions in a floor plan}
\label{fig:floor_plan}
\end{figure}

In the pre-processing stage we breaking down the floor plan into regions based on geometry like rooms and corridors as shown in figure \ref{fig:floor_plan}. 
This can be done manually or by automated geometric analysis. 
Typically only the regions are made nodes of a graph but in this case we make both the regions and the associated entry/exit points of regions as nodes of the information graph.
We do maintain the type of node they are, assign edges between nodes of a region and it's entry/exit point nodes as shown in figure \ref{fig:floor_graph}.
It is very important to keep the entry/exit points as nodes as well instead of directly connecting from one region node to another. It allows the global planner
to dis-ambiguously know in which region the MAV currently is based on currently followed edge. If we used direct region to region edges it is very difficult to identify when the MAV switches region
and it is very critical as this is when the uncertainty of position really matters. This helps optimizing the local planner by focusing on switching of regions 
and maintain/gain the required uncertainty instead of overworking and trying to achieve minimum uncertainty all the time. 
Also the global planner gives better direction to local planner as now it can now direct towards an exit point of an region instead of a node inside next region which may go through walls.

\begin{table}
\rule[0.5ex]{1\columnwidth}{1pt}
  \textbf{Algorithm 1: Generating Information and Search Graphs}
  
  \textbf{Input: } Information about room and doors in accordance to the floor plan
  
  \textbf{Output: } Search-able graph for topological planning
  
  \rule[0.5ex]{1\columnwidth}{1pt}
    \begin{enumerate}
    \item Build information graph node set $ { In_i }$ containing all room and doors, with doors/rooms appropriately marked
    \item Create edge set ${Ie_{ij} }$ between nodes $(In_i ,I n_j )$ if $In_i$ is a room and $In_j$ is a door for the room.
    \item For each room node  $n_i$ add information about boundary wall for that room.
    \item Build search graph node set $ { Sn_i }$ by choosing only the nodes from $ { In_i }$ that are doors
    \item Create edge set ${Se_{ij} }$ between nodes $(Sn_i , Sn_j )$ if $Sn_i$ and $Sn_j$ share a common room node $In_i$  in the information graph
    \item Find Cost and information gain for each edge $(Sn_i , Sn_j )$ by simulating Error propagation and Information gain in the room containing edge $(Sn_i , Sn_j )$ 
    on a PRM based search graph generated in the room.
    \item if the Cost for and edge  $(Sn_i , Sn_j )$ comes as infinite, meaning traveling along that edge is infeasible then that edge is removed from the list.
    \item if no edge  $(Sn_i , Sn_j )$ exists for room node $ { In_i }$ then the room node $ { In_i }$ and edge  $(In_i ,I n_j )$ 
    and door nodes $ { In_j }$ are also pruned from the information graph
    \end{enumerate}
  \rule[0.5ex]{1\columnwidth}{1pt}
  \caption[Algorithm 1: Generating Information and Search Graphs]{}
\end{table}

  \begin{figure}[!h]
   \begin{center}
    \includegraphics[width=1\textwidth]		{nsh_2nd_floor_plan_graph.png}
    \caption{Search Graph with red region nodes, yellow entry/exit nodes and green connecting edges}
    \label{fig:floor_graph}
  \end{center}		
\end{figure}
In the information graph each on the region nodes also boundary wall information for that region. 
We use this to calculate the which regions the MAV can visit and which regions are too small for the MAV.
We then generate a search graph where we eliminate the region nodes and add direct edges between 
entry/exit points of the edges directly. To these edges we add the cost for distance needed to be traveled
uncertainty transfer along the path. We do this by analyzing the original information graph region node meta-data. 
We generate random sample points in collision free area and connect to other neighboring points in straight line collision free path
to generate PRM graph. On this PRM graph we use A* with euclidean distance heuristic cost to search for a 
optimal path from the source entry/exit point to the destination entry/exit point of that room. 
Particle filter is used simulate propagation of error and gain of information for walls within range for sensors. 
Find the the lowest cost path that starts with highest possible uncertainty possible for that starting door and 
ends with uncertainty lower than maximum allowed an the exit door as shown in figure \ref{fig:gplan}. 
Store the cost of the path and error propagation for that edge in search graph.


\begin{table}
\rule[0.5ex]{1\columnwidth}{1pt}
  \textbf{Algorithm 2: Find Cost and information gain for each edge in Search Graph}
  
  \textbf{Input: } Information Graph and Search Graph
  
  \textbf{Output: } Cost and information gain for each edge in Search Graph
  
  \rule[0.5ex]{1\columnwidth}{1pt}
    \begin{enumerate}
    \item For each room node  $In_i$ in information graph generate random sampled points in 
    free space and connect the neighboring points if the straight line points are also in free space exactly like PRM
    \item For each edge $Se_{ij} $ corresponding to room $In_i$ we try to find an optimal path from $Sn_i$ to $Sn_j$.
    \item Use A* with euclidean distance heuristic cost to explore the graph while trying to find an optimal path.
    \item Use particle filter along the search path to propagate uncertainty and incorporate information gain. 
    Use visible portion wall within sensor range to gain information and reduce uncertainty. 
    \item Start the search with the highest possible uncertainty possible to enter the room using door $Sn_i$ for path edge $(Sn_i , Sn_j )$
    and end search when lowest cost path has been found that satisfy the maximum exit uncertainty allowed to exit room using door $Sn_j$.
    Store the cost and  uncertainty propagation function in $(Sn_i , Sn_j )$.   
    \end{enumerate}
  \rule[0.5ex]{1\columnwidth}{1pt}
  \caption[Algorithm 2: Find Cost and information gain for each edge in Search Graph]{}
\end{table}

Then we make the topological plan is generated by simply using POMDP on the search graph. 
While doing POMDP we take care of the growth of uncertainty while traversing through the search graph and also the maximum uncertainty
constraints to transfer from one node to another.
POMDP is more complete than a simple search as it allows the MAV to get lost in intermediate 
steps as long as it can still find it's way back to the destination.
POMDP is also useful for cases when in real case the MAV can become more uncertainty than 
anticipated while planning due to hostile environment (smoke,darkness), as it allows the planner to 
maintain multiple beliefs instead of a single current location. The plan also depends on the current level of confidence/uncertainty and position.



In the query phase the local planner follows the global plan as shown in figure \ref{fig:topo} and  provides updates on real situation like whether an region 
is accessible  or an entry/exit point is blocked. Thus the global planner deletes the associated edges and regenerates the plan. The successful transition 
between regions through entry/exit points are also detected by local planner based on its perception and notified to the global planner which keeps a track 
of its current location necessary to dynamically update plans. This way the drift is be corrected at every transition. 
If the robot does not have enough confidence to make a transition 
like in corner with multiple doors and high uncertainty, it would dynamically re-plan to gain more information and come back to do the transition to reach it's goal.
The global planner maintain it's own separate location and uncertainty using a particle filter. The particle filter motion is taken from state estimation UKF.
Particle filter uses the wall information from current RGB-D scan and compares it to the floor-plan to gain information about its location. It also uses state estimation
location to gain information about location so that particle filter in global planner and state estimation UKF stay well converged. The particle filter also uses the 
successful room change information from the local planner to gain information. The localization from particle filter is also sent to the UKF as simulated GPS input
to make the entire localization system benefit from the floor plan. A separate localization estimate is maintained using particle filter in global planner
have the possibility to maintain multi-modal values and use POMDP to the full extent making the global planner very robust and recovery high uncertainty possible.


 \begin{figure}[!hp]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{topo.png}
		\caption{Topological plan being executed by local planner}
		\label{fig:topo}
		\end{center}		
\end{figure}




% Global planner 
% Balance cost and uncertainty to destination
% Minimize / Threshold ?
% Throughout path / Goal ?
% Overcome local minima of uncertainty efficiently.
% 
% plan on regions  broken up by geometrical information provided on the floor plan. Generate more regions if needed based on information content and associated cost to retrieve information.
% 
% cost functions for changing rooms. entry and exit cost / transition cost.transition uncertainty requirements
% 
% region cost would computed only once in the beginning and may be updated later if required when traversing that region.
% 
% 
% using RRBT efficiently	with uncertainty minimization in brm heuristics is hard to use	
% try using A* in RRBT as it has only uncertainty threshold, and looks for minimum cost		
% 
% a path would be chosen based on current uncertainty and thus there is no single best road-map.
% most planning algos are optimise's for a open space with obstacles. topological planning uses geometrical planning
% 
% \cite{Kaelbling:95} Planning and acting in partially observable stochastic domains. 1998
% In the case of discrete states, actions, and observations, exact Partially Observable Markov Decision Process (POMDP) algorithms exist, but are computationally intractable for realistic problems
% 
% \cite{lavalle06} Probabilistic Road Map (PRM)		PRM: learning by randomly generating points in qfree and connection k closest if edge in qfree and then in query phase join user defined end points 																	
% The Probabilistic Roadmap (PRM) is a common algorithm \cite{Kavraki96probabilisticroadmaps} 
% for planning in high-dimensional problems, in
% which a discrete graph is used to approximate the connectivity of Cfree
% If the UAV executing the plan does not have a good estimate of its state, it may not be able to
% determine when it has arrived at a graph node and should
% start to follow a new edge.
% 
% 
% Rapidly-exploring Random Tree (RRT)		RRT:operates by growing a tree in state space, iteratively sampling new states and then "steering" the existing node in the tree that is closest to each new sample towards that sample. The RRT has many useful properties including probabilistic completeness and exponential decay of the probability of failure with the number of samples																	
% 
% Belief RoadMap (BRM)
% BRM: addresses observability issues by simulating measurements along candidate paths and then choosing the path with minimal uncertainty at the goal. Dynamic constraints Ignored																	
% 
% Rapidly-exploring Random Graph (RRG) 
% Karaman and Frazzoli	RRG: is an extension of the RRT algorithm. In addition to the nearest connection, new samples are also connected to every node within some ball.																	
% 
% RRT*	Karaman and Frazzoli	RRT*: converge to the optimal path by only keeping the edges in the graph that result in lower cost at the vertices with in the ball.																	
% Rapidly-exploring Random Belief Trees  (RRBT)	Adam Bry, Nicholas Roy	RRBT: extend the pruning strategy for dynamic systems (i.e. a non deterministic mean) and also use the pruning strategy in the context of an incremental algorithm to terminate search. The basic idea of RRBT is to interleave graph construction and search over the graph																	


% \begin{table}
% \rule[0.5ex]{1\columnwidth}{1pt}
% 
% \textbf{Algorithm 1: Emergency Maneuver Trajectory Set Application
% for Reactive Safety}
% 
% \textbf{Input: }$ $Safety library at previous timestep $\Phi_{Previous}$,
% Currently executed trajectory $\sigma(t)$
% 
% \textbf{Output: }Command trajectory $\gamma(t)$ , Safety library
% at current timestep $\Phi_{New}$
% 
% \rule[0.5ex]{1\columnwidth}{1pt}
% \begin{enumerate}
% \item $\Phi_{New}=\{\emptyset\}$.
% \item $s_{t}=\sigma(t)$
% \item \textbf{for $\forall\phi_{c}\in\Phi(s_{t})$}
% \item \quad{}\textbf{if $\Omega(\phi_{c})\subseteq K_{t}/O_{t}$}
% \item \quad{}\quad{}$\Phi_{New}=\{\Phi_{New},\{\phi_{c}\}\}$
% \item \quad{}$\quad$$\quad$\textbf{endif}
% \item \textbf{endfor}
% \item $\quad$\textbf{if} $\Phi_{New}=\{\emptyset\}$
% \item \quad{}$\quad$$\quad$$\gamma(t)=\phi_{e}\in\Phi_{Previous}$$\quad$
% \item \textbf{else}
% \item \quad{}$\quad$$\quad$$\gamma(t)=\sigma(t)$$\quad$
% \item \textbf{endif}
% \end{enumerate}
% \rule[0.5ex]{1\columnwidth}{1pt}
% \end{table}

% \begin{verbatim}
% \STATE <text>
% \end{verbatim}
\clearpage
\section{Local Planner}
\label{section:plan_local}
%Most planners are made for open space with obstacles.
Local planner has many parts, mainly Grid mapping, Distance Transform, Cost functions and trajectory optimization.



\textbf{Grid Mapping} Using the RGB-D and pose information we first accumulate the obstacles found in three dimensional grid know as grid map.
This is done in two stages, first we make an evidence map when we increase the probability of a obstacle as we get more 
positive hits in that location and decrease evidence value as we get negative hits (positive hits  beyond that point in the same line of sight).
When an threshold evidence value is reached we declare it as an obstacle or not an obstacle. Gird points that never got any updates stays as unknown region
which is also very important information for planning. We use  the FOV and range of the sensor to properly maintain the unknown from the know region.
Due to noisy/drifting state estimation we need to keep clearing the grid. going through the whole grid for decaying evidence values can be very costly and slow.
We add the updates in a queue with their timestamps and later take out elements from the queue at regular intervals and undo the update to decay the evidence values.
There is no searching required as the queue is already sorted by the time-stamp.



\textbf{Distance Transform} Distance transform is required to easily find distance from a location to the closest 
obstacle that is required while generating the plan. Generating Distance Map every time for updated grid map is very costly. 
We have implemented and efficient iteratively updated three dimensional distance mapping as proposed by \cite{ScheFergSing09:EfficC-spacostfunctupdat3Dunmanaeriavehic} and show in figure \ref{fig:dt}.
 
 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=1\textwidth]		{local_map.png}
		\caption{Distance Transform on obstacle over time on real flight data}
		\label{fig:dt}
		\end{center}		
\end{figure}

\textbf{Cost Functions}
The local planning have multiple objectives and for each of them we associate a cost function to get a plan optimized for the the objectives with their respective weight-ages.
The primary cost function is Grid-cost function which gives us the cost of each point of the trajectory from the closest obstacle. This is used to avoid obstacles. Directly using Grid-cost 
may affect the trajectory at particular points producing a spiky trajectory, and to overcome this problem we use Gaussian like distribution to spread the Grid-cost over the trajectory.

We also use costfuntion for smoothness of the trajectory, which makes the trajectory more energy efficient and also dynamically feasible. 
We use the Neighbor-effect smoothness cost function as compared with  some other implementations in figure \ref{fig:neighborEffect}. This cost function takes three consecutive points in a 
trajectory and tries to move the central point closer to the centroid formed by the three points and also try to move the other two point parallel in the opposite direction. 
Sometimes the central point  cannot move as it is close to an obstacle and in those cases this function still achieves smoothness as it moves the neighboring points in the opposite direction.


 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=1.1\textwidth]		{neighborEffect.png}
		\caption{Trajectory optimized using different smoothness cost functions compared}
		\label{fig:neighborEffect}
		\end{center}		
\end{figure}


We also have an information gain cost function which induces action like wall hugging and balance the yaw direction so that location information 
can be gained from the wall close by and  it can still see in the direction of the motion to avoid obstacles. While taking turns or moving 
through a door there are shadow regions which stayed unknown in the evidence map, and information gain cost induces action to turn in those directions to gain information
to know the environment better and move safely through turns and doors.

\textbf{Trajectory optimization}
Initial seed trajectories are generated in direction of target(s) provided by the global plan. When we  are close door 
or turning we have two targets, the position of the door/turning and the next node from there in case we can make the transition. 
On these initial seed trajectories we apply cost function find cost of the trajectory and apply the gradient of the cost and apply 
it on the trajectory iteratively to produce many instances of trajectories. We then select the best trajectory with the least cost and send to the trajectory controller. 
The optimization of trajectory based on many cost functions and their gradients as shown in figure \ref{fig:trajopt} and \ref{fig:trajopt_t}. 

 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{gradient.png}
		\caption{Trajectories generated by gradient pushing away from wall}
		\label{fig:trajopt}
		\end{center}		
\end{figure}

 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{gradient_top.png}
		\caption{Path optimized towards center of walls and smoothly by trajectory optimizer}
		\label{fig:trajopt_t}
		\end{center}		
\end{figure}
% 
% 
% planning algorithms
% that are able to efficiently re-plan when the map and associated
% cost function changes. However, much less attention has been
% placed on efficiently updating the cost function used by these
% planners, which can represent a significant portion of the
% time spent re-planning. In this paper, we present the Limited
% Incremental Distance Transform algorithm, which can be used
% to efficiently update the cost function used for planning when
% changes in the environment are observed. Using this algorithm
% it is possible to plan paths in a completely incremental way
% starting from a list of changed obstacle classifications
% 
% 
% 
% Maximize information gain required for local control
% Minimize coupling with global planning
% Predictable uncertainty of local plan outcome
% 
% The Belief Roadmap: Algorithm
% \begin{itemize}
%     \setlength{\itemsep}{15pt}
%     \item  Using PRM sampling strategy to build belief graph node \{$n_i$\} representing means
%     \item  Create edge set $ \{e_{ij}\}$  between nodes ( $n_i$ , $n_j$ ) if the
% straight-line path between ($n_i$, $n_j$) is collision-free
%     \item  Build one-step transfer functions \{$\zeta_{ij} \} \forall e_{ij} \in \{e_{ij} \}$
%     \item  Search for path with minimum uncertainty at goal in Belief graph G = \{\{$n_i$ \}, \{$e_{ij}$ \}, \{$\zeta_{ij}$ \}\} 
%     \end{itemize}
% 
% 
% 
% sample-at
% returns points halfway to previous point in trajectory and next point.
% weightage is also assigned as max of the two halfway  sides of the trajectory, decreases by 1 as we move outward
% Cost functions 
% GridObstCost
% Cost
% average of all point suing sample-at for a trajectory point, without using weightage.*velocity*scaling
% gradient
% for each point in trajectory, for each sample of sample at, perturb x,y,z separately by +1, get difference of dist. Gradient sum of (inverse of difference scaled by 200 and * weight
% age) * scaling* velocity
% costOptimistic
% costPessimistic
% 
% 
% energy cost	obstuse tracj geneter gradient vector outside the concave area	try other methos like mid point, length ratio, curvature		centroid with distribution to neighbors worked	
% 
% 
% 
% EnergyCost
% Cost
% sum of length of gradients gradients
% gradient
% lenght(x2-x1)*direction(x2-x0) – (x2-x1) for all the points of trajectory
% 
% DsitributionCost
% Cost
% returns original cost of trajectory
% gradient
% Distribute the cost gradient using a finite gaussian like distgribution.
% 
% AdditiveCost
% add cost taking care of overflow
% 
% Initial Guess
% FixedDirectionDiscretizedGuess
% 
% TurnDiscretizedGuess
% 
% StraightLineDiscretizedGuess
% 
% 
% 
% 
% TrajectoryOptimization
% 
% 
% Optimize
% Calculate gradient over the trajectory using the cost fuction.
% Aplly gradient on the trajectory with lambda step. find the cost of the new trajectory
% System dynamics can be used to get predicted path and calculate cost of path upon that.
% \section{Cost functions}
% \subsection{Grid Obstacle Cost}
% Sum of closest obstacle distance from each point on the trajectory scaled with some weight-age and velocity.
% %cost =Average of all point suing sample-at for a trajectory point, without using weight-age.*velocity*scaling. \\
% %Gradient = For each point in trajectory, for each sample of sample at, perturb x,y,z separately by +1, get difference of dist. Gradient sum of (inverse of difference scaled by 200 and * weight
% %age) * scaling* velocity\\
% 
% %function: sample-at()\\
% %returns points halfway to previous point in trajectory and next point.
% %weight-age is also assigned as max of the two halfway  sides of the trajectory, decreases by 1 as we move outward
% 
% \textbf{Changes required}\\
% Cost is calculated using closest obstacle returned by distance transform. 
% This may not always make sense, obstacle parallel to the trajectory is much 
% less important that obstacle on the path of the trajectory perpendicular to the direction of trajectory. 
% To solve this back tracing the trajectory and rewiring the obstacle affecting each point in the trajectory may be better.
% 
% \textbf{Changes made}\\
% Cost affecting the path trajectory required a very high value close to the obstacle to push it away and safe due to uncertainty in motion and controller. 
% But also required to not affect the path much that is safely away from the obstacle to  make the path less sensitive to far way obstacles. 
% This requirement is more prominent due to the small space in corridor environments. The cost now goes down more drastically as we move away from it.
% %Also fixed the gradient calculation which was previously done using squared value.
% 
% 
% 
% %\subsection{Energy-cost}
% %Cost = Sum of length of gradients at each point of trajectory\\
% %For 3 consecutive points $x_0, x_1 & x_2$, $x_1$ being the current point on trjactory, 
% %gradient = length$(x_2-x_1) *$ direction$(x_2-x_0)-(x_2-x_1)$ \\
% \subsection{Distributed Centroid Cost}
% This cost function tries to smoothen the the trajectory. Of every consecutive 3 points the middle point tries to go toward the centroid. 
% The distribution makes the neighboring point to follow a fraction of the gradient of current point in the opposite direction. This distribution
% gives the capability to smooth the trajectory around sharp edged obstacles.
% 
% \subsection{Distribution Cost}
% Distributes existing cost using a finite Gaussian like distribution. 
% This makes obstacles affect the trajectory at the closest point and 
% their neighbors with a smaller weight-age instead of affecting a single 
% point with a high value.
% 
% \subsection{Additive Cost}
% Sums up other cost functions taking care of overflow
% 
% 
% \subsection{Info-gain}
% New cost function to predict information gain along path from current view. 
% This would prevent going to close to walls. Prediction can be done by removing 
% information from the grid and visual odometry matching according to predicted path. 
% Depth detection can also be simulated according to sensor properties on the previous 
% cloud and information gain can thus be predicted. 
% 
% Yaw control has to do the rest of the job to actually gain the more information like looking 
% at good geometry while going through corridor or looking for information in unknown space.
% 
% \section{Initial guess}
% Based on the behavior and direction the simplest straight line paths are generated by initial guess.
% The initial pose for this trajectory is sometimes taken as the actual pose from state estimate 
% or a projected look ahead point on the previous trajectory.\\
% \textbf{Changes required}\\
% Have already changed the lookahead point selection with is different from the algorithm used in trajectory controller. 
% The look ahead point keeps on moving forward as it uses the last trajectory which was based on the previous look ahead point.
% This can be improved using fixed distance (delay * speed) from projected point on extrapolated trajectory.
% 
% \section{Trajectory Optimization}
% Calculate gradient over the trajectory using the cost function combination.
% Apply gradient on the trajectory with lambda step. find the cost of the new trajectory. Trajectory with minimum cost is finally chosen for execution
% System dynamics can be used to get predicted path and calculate cost of path upon that. Currently using only XY optimization.\\
% \textbf{Changes required}\\
% Need to do complete XYZ optimization instead of XY optimization, and this would also require 
%  ground plane estimate  to maintain altitude.
% Ground plane information may also be used for initial guess.
% 
% CHOMP optimizes cost of a trajectory which include cost of closest obstacle distance from signed 
% distance transform and a smoothness cost which is given by sum of squared derivatives \cite{Ratliff_2009_chomp} .
% 
% 
% 
% \section{Grid-mapping and Distance Transform}
% Maintains an evidence grid with values -127 to 127. Value -128 is unobserved. Transition from $<=0$ to $>=1$ is observed as addition of obstacle and vice-versa.
% List of obstacle additions and deletions are published to be used by Distance Transform. Evidence grid values are also used by yaw control to look for unobserved cells.
% Distance Transform maintains the distance to the closest obstacle.\\
% \textbf{Minimal Changes required}\\
% Unobserved cell with value -128 should behave like 0 while adding/subtracting. \\
% Evidence grid values should decay over time which may require full cell space sweeps. \\
% \textbf{Major Changes proposed}\\
% Even with decay obstacles and clear space are binary states to the distance transform.
% We could maintain last time-stamp for every cell update. The cells can not only decay probability 
% of obstacle/free space, they can also grow in occupancy region with time due to state uncertainty of the robot. 
% This would also save the robot hitting old obstacles due to drift
% Eventually the the cells would decay to unobserved space. 
% Decay rate can be a function of trace of recent state estimation uncertainties.
% 
% 
% \section{Yaw Info Gain Control}
% This module would use directional information from the Global plan executor and try to optimize 
% the yaw not only for the current next node but also for the next to next node so that when it 
% needs to make a turn it can look for the opening and be more ready for the turn. 
% 
% The Yaw controller would also control the velocity based on the trajectory, current position, 
% obstacles and unobserved cells in the proximity so that it can be safer by 
% more like low level behavioral control to stop it from bumping into obstacles.
% 
% It would also control the yaw at different position on the trajectory trying to gain more information
% like tracking key-points, good matches, and also based on timestamps and unobserved cells in grid-map.
% This should also consider turn rate and if required motion blur and distance of obstacles 
% in each direction to avoid going closer than minimum range of sensor.
% \subsection{Information Gain}
% Hence, at any moment, full information about the surrounding environment is only available as 
% a function of the current vehicle state and prior motions. 
% 
% RGBD infogain 
% field of view is small and featurefull elemets may stay out of view 
% can either try to keep in center so that all 4 walls a visible, or try 
% to track features and direct the caemra towrds the side with more visual(+depth) 
% features to get better match and thus better state estimation. Can also do something 
% similar for Imu excitation.  This might not work in local trajectory controller and 
% may need to be more dynamic like yaw control. also needs to know which direction to 
% go next. need info from matcher node of libviso
% \subsection{Obstacle Mapping}
% 
% Grid Mapping 
% Distance Transform	
% 
% Gridmapping decay
% plan is to decay obstacles and free space over time to unobserved space and to do 
% it efficiently we may or may not use iterations over all to update the evgrid with 
% updated values. Updation inchange of state of a cell need to be passed to IDT. 
% We may not want to do DT over unobserved space as obstacle, instead just use 
% that in yaw control level to cut out trajectories and keep the robot safe. We 
% can also track state estimation uncertainty over time and attach it to each cell 
% updated time and thus stay away more distance from old obstacle as their location 
% is more uncertain and we may try to regain their information agian when it turns 
% unobserved. also we have to predict state estimation uncertainty along a 
% trajectory using VO matching quality and quatiticy, but looking at the geometry 
% and features available and optimize the path accordingly