all : bib pdf

pdf : bib *.tex
	pdflatex MTP_Thesis_12CS60R32_Jit.tex

bib : bibliography.bib
	pdflatex MTP_Thesis_12CS60R32_Jit.tex
	bibtex MTP_Thesis_12CS60R32_Jit
	
clean : 
	rm -f *.log
	rm -f *.aux
	rm -f *.blg
	rm -f *.bbl
	rm -f *.toc
	rm -f *.lof
	rm -f *.lot
