\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{iitkgpthesis}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
\LoadClass[a4paper,12pt]{book}
\RequirePackage[a4paper,inner=3.75cm,outer=3cm,top=3cm,bottom=3cm,pdftex]{geometry}
\RequirePackage{rotating}
\RequirePackage{graphicx}
\RequirePackage{natbib}
\RequirePackage{lscape}
\bibpunct{(}{)}{; }{a}{}{, }

\RequirePackage{setspace}
\onehalfspacing
%\doublespacing
\setlength{\parskip}{12pt}

\renewcommand*\arraystretch{1.5}
%\RequirePackage{fancyhdr}
%\pagestyle{fancy}

\RequirePackage[hang,small]{caption}
\RequirePackage{multirow}
\RequirePackage{enumerate}

\renewcommand{\@makechapterhead}[1]{%
\vspace*{50\p@}%
{\parindent \z@ \raggedright \normalfont
\ifnum \c@secnumdepth >\m@ne
\huge \@chapapp\space \thechapter % Chapter number
\par\nobreak
\vspace{-10pt}
\fi
\interlinepenalty\@M
\Huge \bf #1\par % chapter title
\nobreak
\vskip 40\p@
}}

\renewcommand{\@makeschapterhead}[1]{%
\vspace*{50\p@}%
{\parindent \z@ \raggedright
\normalfont
\interlinepenalty\@M
\Huge \bf #1\par % chapter title
\nobreak
\vskip 40\p@
}}



\newenvironment{dedication}
  {\cleardoublepage \thispagestyle{empty} \vspace*{\stretch{1}} \begin{center} \em}
  {\end{center} \vspace*{\stretch{3}} \clearpage}


\newenvironment{psuedochapter}[1]
  {\cleardoublepage
   \thispagestyle{empty}
   \vspace*{\stretch{1}} 
   \begin{center} \large {\bf #1} \end{center}
%   \addcontentsline{toc}{chapter}{\numberline{}#1} --not yet mjz 
   \begin{quotation}}
  {\end{quotation} 
   \vspace*{\stretch{3}}
   \clearpage}

\newenvironment{abstract}
	{\begin{psuedochapter}{Abstract}}{\end{psuedochapter}}

\newenvironment{acknowledgments}
	{\begin{psuedochapter}{Acknowledgments}}{\end{psuedochapter}}
	


% A simple style file to provide a draft marking in the background of
% a file
% Thank you to Will Uther who provided the base code that I rolled
% into this package
% ChangeLog
% 1.1: shrunk and shifted the stamp so that it avoid the unprintable
% area at the top of the page
% 1.2 dkoes - made it big and rotated and light
% 1.3 dkoes - made it small, and unrotated since Jonathan Aldrich complained
\RequirePackage{graphicx}
\RequirePackage{eso-pic}
\RequirePackage{color}

%Got this from Will (will@cs.cmu.edu)
%usage: \draftstamp{date}{label}
\newcommand{\draftstamp}[2]{\definecolor{DraftGrey}{gray}{0.7}
  \ClearShipoutPicture\AddToShipoutPicture{
    \begin{minipage}[b][\paperheight][t]{\paperwidth}     
    \centering
    	\vspace{.4in}
            \textcolor{DraftGrey}{\renewcommand{\baselinestretch}{1.0}\normalsize
            \begin{tabular}{c}
                    #1 \\
                    #2
             \end{tabular}            
        }
        
    \end{minipage}
}}
\endinput
