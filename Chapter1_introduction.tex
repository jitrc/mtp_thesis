\chapter{Introduction}
\label{chap:intro}
\minitoc



There are vast amount of applications for autonomous Micro Aerial Vehicles (MAV). These applications 
include search and rescue, surveillance, exploration and others.  Outdoor use of MAV have 
been heavily researched upon. Physical capabilities of the MAV makes it very suitable 
for many such applications than any ground robot as shown in Figure \ref{fig:disaster}.
MAV can easily help in these situations being easily deployable, small enough to enter 
building through broken windows or and other small openings irrespective of terrain conditions.
These applications require the vehicle to be able to operate 
in indoor environment and sometimes the environment may also be hostile and unstructured. 

\begin{figure}[ht]
\subfloat[Flood]{\label{figur:1}\includegraphics[width=0.50\linewidth]{flood-660_061713110720.jpg}}
  \subfloat[Fire]{\label{figur:2} \includegraphics[width=0.45\linewidth]{Fire_PTI1.jpg}}
 \caption{Buildings rendered inaccessible for humans and ground robots}
\label{fig:disaster}
\end{figure}

To make MAV usable and more useful we have to take up a few challenges. 
We have to make the MAV fully autonomous such that it can deployed in an infrastructure 
free environment and can be used without and supervision during operation and without the need of any network connectivity.


\section{Challenges}
To have full autonomy in indoor mission we need state estimation without 
the availability of GPS. State estimation alone may not be enough for the 
MAV from getting lost as we have no external input like GPS and state estimation 
would drift and uncertainty would increase over time.  If the planner plans to reduce 
drift and uncertainty it would improve the performance and safety of MAV by a great extent.\\


\textbf{Limited Sensing Payload}: 
Small vehicle limits the payload capacity to less than 1.0 kg, so weight is a 
strong consideration when designing the sensors for the vehicle.
Due to power and payload constraint we are limited to senors like lasers and 
cameras (mono, stereo, or RGB-D) for perception.

\textbf{Limited Onboard Computation}: 
Due to power and payload constraint choice of computation platforms are also limited.
Still we have depend only on the onboard computation capabilities and we do not want to depend
on any external infrastructure while achieving full autonomy.

\textbf{Indirect Position Estimates}: 
For Position estimation we cannot use GPS as they do not work reliably enough if at 
all in indoor environment. Neither can we use any direct method 
like wheel encoder which touches the environment. We have to reply upon inertial 
measurement from \ac{IMU} and Visual odometry. As there is no absolute measure of position
the pose estimation may drift a lot over time and path planner need to aware of that and 
try to minimize the possible drift or reduce drift  when possible required for successful completion of mission.


\textbf{Fast Dynamics}: 
Unreliability in state estimation or controller can make the MAV go crazy crash hurting itself and others around. There is no safe state like 
in ground vehicle where the planner can issue a 0 velocity command when things go out of control. In a MAV for 0 velocity the state estimation
and controller have to work, and work fast to keep the MAV hovering at the same place. This is not an easy task with not external sensor. 
An additional delay 100ms in perception-control channel may change the quality of control drastically.

\textbf{3D Motion}: 
 For the operation of autonomous micro-air vehicle (MAV) in indoor environments 3D information of the environment  is required for 
collision-free path planning.  Indoor environment have more obstacles and closer obstacles that most outdoor environments where the MAV needs to operate
and thus a good 3D mapping of obstacles is required to keep both environment and MAV safe. Limited range of sensor, limited computation power and
noisy state estimation combined makes 3D mapping and 3D motion planning difficult. We also need the 3D Mapping and 3D motion planning to be fast 
and responsive to changes in the environment.


\textbf{RGB-D senor range limitation}: 
 RGB-D senors are popular in robotics community being very affordable and light weight and information rich sensor.
They have been used previously on MAVs for mapping and localization. It is appropriate to use in our case as 
it can also sense the environment in the dark and in hostile conditions like through smoke.
RGB-D sensors are range-limited on both the higher and lower end. Lower end limit is  
problem in small constrained space and needs special consideration in planing. The higher end limit 
is not a big problem as the vehicle would normally move slowly in indoor environment but sate estimation
in large open space may be a problem. Also the accuracy of the depth data provided by 
RGB-D sensors deteriorates with the distance from the sensor.


% ---------------\\
% 
% For the operation of autonomous micro-air vehicle (MAV) in indoor environments 3D information of the environment  
% is required for collision-free path planing and useful for position and velocity estimation. RGB-D cameras are good 
% for the purpose as they are light and low-cost and can be used indoors. One problem with these RGB-D sensors are that they 
% are range-limited on both the higher and lower end. While range limit on the high end may not be a problem for slow moving 
% vehicle but lower end of the range limit certainly needs special consideration when working constrained space as it would be 
% able to sense an obstacle if it is very close. Limited maximum range does pose a challenge when using depth information for 
% position estimate in large open space. Also the accuracy of the depth data provided by RGB-D sensors deteriorates with the distance 
% from the sensor. Considering these properties of the sensor is important to make the trajectories safer and also to complement 
% the capability of the senor to stay more localized.

\section{Related Work}
\label{sec:related}
Autonomous indoor operation of MAV needs to solve many sub-problems. First we need a stable controller
that works with the available state estimation and produce preferred hovering and velocity control.
Then we need a local planner that utilize immediate perceived information to stay safe by 
avoiding obstacle in its way and try to gain the required information to stay safe locally and 
be localized more accurately and more confidence globally. Once we have controller and local planner
we need a global planner that can make a plan globally given a mission and floor plan with the 
objective to finish the mission safely.

\textbf{Controller}
%http://teal.gmu.edu/ececourses/ece521/labtutorials/LQR/lqrdesign.html
\cite{Hoffmann10} \cite{argentim2013pid} Gives us nice comparison of \ac*{PID}, \ac{LQR}, \ac{IB} based controllers. 
PID being the simplest for implementation and can be even implemented empirically without a perfect model of the MAV.
\cite{Bresciani2008}  \cite{kis2013navigation} \cite{Bouabdallah2004} \cite{rich2012model} Show methods to generate model parameters 
or the MAV also know as system identification. This method gives us better tuning parameters for the PID controller and more realistic 
simulation model for testing.



\textbf{Local Planning}
\cite{ScheFergSing09:EfficC-spacostfunctupdat3Dunmanaeriavehic} Show as efficient way to maintain a 
distance transform for 3D environment in an iterative method allowing real-time 3D motion planning to be possible.
CHOMP\cite{Ratliff_2009_chomp} optimizes cost of a trajectory which include cost of closest obstacle distance from signed distance transform and a 
smoothness cost which is given by sum of squared derivatives.
Path Planning for Motion Dependent State Estimation on Micro Aerial Vehicles \cite{achtelik2013}, produces plans that 
excites linear acceleration and angular velocity to make all states become observable and improves estimation of system states.
Rapidly-exploring Information Gathering (RIG)  \cite{Hollinger13}  generates  maximally informative trajectories 
for guiding mobile robots to observe their environment. \cite{Scherer_2008_6053} showed online environmental sensing, 
path planning and collision avoidance in three dimensions.


\textbf{Global Planner}
Planning with uncertainty is classically Partially Observable Markov Decision Process (POMDP) 
but cannot be done in real-time for large real world or continuous problems.
For high-dimensional configuration spaces with algorithms like Probabilistic Roadmap (PRM) \cite{kavraki1996probabilistic} 
and  Rapidly-Exploring Randomized Trees (RRT) \cite{lavalle1998rapidly} are very successful.
The Belief Roadmap (BRM) \cite{PreRoy07} based on PRM, solved planning with uncertainty for high-dimension 
configuration space. Incorporates the predicted uncertainty of future position estimates and balances 
shorter paths against reduced belief uncertainty. It thus maximize the probability of reaching the goal.
Rapidly-exploring Random Belief Trees  (RRBT) \cite{BryRoy11} extend the pruning strategy of BRM by considering dynamics of the system.

\textbf{Indoor autonomous MAV}
Similar work in GPS-denied navigation exists using laser-based odometry, 
structured-light or places visual markers, radio beacons in the environment. 
Some of these systems does the computation on-board and some relies on 
communication to external computing resource.

Laser-based state-estimation is a good as it is computationally cheaper than vision-based solutions and also more accurate. 
Planning in Information Space for a Quadrotor Helicopter in a GPS-denied Environments  \cite{HePrenRoy08:PlanninforspacequadrhelicGPS-denvir} 
use the Belief Roadmap (BRM) algorithm and extends it to use Unscented Kalman Filter (UKF).  They use laser range-finder for perception.
Achtelik et. al. \cite{Achtelik09SPIE}  and  Bachrach et. al \cite{Bachrach09IJMAV}shows laser-based localization system for use on a 
small MAVs.  But if no structure is within the sensing range of the laser, state estimation would fail.
This approach does not give us 3D information required for most applications.


Estimation, planning, and mapping for autonomous flight using an RGB-D camera in GPS-denied environments \cite{Bachrach2012}
 We show how the Belief Roadmap (BRM), a belief space extension of the probabilistic roadmap algorithm, can be used to plan
vehicle trajectories that incorporate the sensing model of the RGB-D camera

Huang et. al. \cite{huang2011isrr} present a  method for using a Kinect for creating a map of an indoor environment. 
These sensors are very light and low cost and server the purpose very well.  
But they also hove limitation of being range limited and does not work outdoors.
Bachrach et al. \cite{Bachrach2012} showed the use of BRM with RGB-D cameras extending previous results by given by Huang et al. (2011)
\cite{huang2011isrr}  demonstrating the RGB-D mapping algorithm.

Ok et. al. \cite{ok:Pathplanninguncertainty:VoronoiUncertaintyFields} proposed Voronoi Uncertainty Fields.
This a hierarchical planner that has a top-level planner that forms local waypoints using 
Voronoi vertices's and a bottom-level planner that locally refines the actual path using uncertainty-biased potential fields.

We are proposing a hierarchical planner with top level being topological planning with uncertainty. 
This will utilize the prior floor plan information for segmenting regions for topological nodes 
and predicting information gain in each region. The topological plan can change dynamically 
depending on current state uncertainty and updates on cost of regions.
Bottom level planner takes care or making vehicle safe incorporating the limitations of sensors, 
improving information gain and performing perception by planning to map back to topological 
plan to correct drift when actually required.



\section{Problem Statement}
In the research presented in this thesis, we sought to tackle the the problems described
above and develop a system that integrates planning, and control to enable a MAV
to autonomously carry out missions in indoor environments safely. 
We seek to do this using only limited onboard sensing and computation and with prior 
knowledge of floor plan of the environment used to assign missions to the MAV.
What we want to achieve is reach goal position within bounded uncertainty being safe all the way\\

Safety = $E(x,y,z \in X_{obs})<e$ \\

%$e=0.0001$



\section{Organization}
The thesis consists of four parts: (1) Chapter \ref{chap:system} System overview
that details our approach ; (2) Chapter \ref{chap:control}  System Identification and Controller design and Dynamic simulation; 
(3) Chapter \ref{chap:plan} describes the Global Planning and Local Planning system s 
(4) Chapters \ref{chap:experiments} describes the 
vehicle on which these algorithms were tested and the results of the flight experiments
