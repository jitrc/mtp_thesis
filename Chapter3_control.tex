\chapter{System Identification and controller}
\label{chap:control}
\minitoc

Before we can do dynamic simulation as mentioned is section \ref{section:dynamic_simulation} or 
tune the controller as mention in section \ref{section:velocity_control} we need a model for the 
system and we do it in section \ref{section:sysid}.
The vehicle already have an attitude controller embedded in it that will take as commands a desired roll, pitch, yaw rate and total thrust.  
A cascaded control approach is taken where the inner control loop is a velocity controller that accepts a 
3-dimensional velocity command and produces commands applied to the existing low-level attitude controller.  
The outer loop of the control system achieves trajectory tracking.  This loop receives a desired path from the 
planner and issues commands to the velocity controller to track the path.  The whole system is 
illustrated in figure \ref{fig:system_overview} and the enclosing rectangle on the right side contains all the three low level controllers.


\section{System Identification}
\label{section:sysid}
First thing we do is get a mapping from the input thrust command and the force generated. For doing that we tie down
 the robot on a weighing scale and measure the thrust generated for the full range of input values as show in the table \ref{tab:thrust}. Then we fit a line 
 using regression as shown in figure \ref{fig:ThrustCurve}. For the regression line $Y = m*X + b$, $Y$ being the Uplift and $X$ the thrust command we try to 
 estimate the slope $m$ and offset $b$ as shown in the equation \ref{eq:thrustCurve}. 
 \begin{table}[!ht]
  \centering
 	\begin{tabular}{|c|c|}
	\hline
	      \textbf{Thrust} [$0-250$](X)	& \textbf{Uplift} [$kg*m/s^2$](Y) \\		
	      \hline
		0	&	4.1202\\
		10	&	4.1202\\
		20	&	3.924\\
		30	&	4.5126\\
		40	&	5.6898\\
		50	&	7.4556\\
		60	&	8.4366\\
		70	&	10.2024\\
		80	&	11.772\\
		90	&	13.734\\
		100	&	15.696\\
		110	&	17.8542\\
		120	&	19.8162\\
		130	&	22.1706\\
		140	&	24.7212\\
		150	&	27.2718\\
		160	&	30.2148\\
		170	&	33.7464\\
		180	&	36.4932\\
		190	&	39.0438\\
		200	&	42.3792\\
		210	&	44.5374\\
		220	&	45.9108\\
		230	&	49.05\\
		240	&	47.6766\\
		250	&	50.031\\
	      \hline
	      \end{tabular}
    \caption{Measured uplift corresponding to thrust values}
  \label{tab:thrust}
\end{table}
 

 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.8\textwidth]		{ThrustCurve.png}
		\caption[Thrust Curve]{Thrust Curve line fitting from collected data}
		\label{fig:ThrustCurve}
		\end{center}		
\end{figure}

 \begin{equation}
 \label{eq:thrustCurve}
 \begin{split}
	m = cov(X,Y) / var(X)\\
	b = Y/(m*X)
  \end{split} 
\end{equation} 


 
 
After getting the Thrust curve and thrust offset we can make a controller to generate thrust for a close hover. 
The robot would still not hover due to disturbance to which the robot needs to react by controlling roll and pitch.
For this we need to model the attitude response. There are many unknowns to model the attitude response directly.
We simplify this by making some assumptions. The first assumption made is that the roll, pitch
and yaw dynamics are independent as the low-level control system will be attempting
to minimize the coupling effects, so it is acceptable to neglect them.  The decoupled attitude system in Eq. \ref{eq:decoupled_attitude} with
the notation shown in Table \ref{tab:attitude_variables}.


\begin{equation}
\label{eq:decoupled_attitude}
	\begin{cases}
	\phi_t &= f_\phi(\phi^d_{0:t})\\
	\theta_t &= f_\theta(\theta^d_{0:t})\\
	{\dot\psi}_t &= f_{\dot\psi}({\dot\psi}^d_{0:t})\\
	\end{cases}
\end{equation}

\begin{table}
\centering
	\begin{tabular}{| r | l |}
		\hline
		Symbol & Definition\\
		\hline
		$\phi^d$ & desired roll angle\\
		$\theta^d$ & desired pitch angle\\
		${\dot\psi}^d$ & desired yaw rate\\
		\hline
	\end{tabular}
	\caption{Attitude Model Variables}
\label{tab:attitude_variables}
\end{table}


The second assumption made is that $f_\theta$ and $f_\phi$ can be 
represented as two-dimensional state-space systems and $f_{\dot\psi}$ can be represented as a 
one-dimensional state-space system to the form shown in Eq. \ref{eq:attitude}.  
By presuming a state-space model of the dynamics, linearity in the attitude response is assumed.  
This assumption is not valid for extreme attitudes, but remains a good approximation near hover.
\begin{equation}
\label{eq:attitude}
	\begin{cases}
		\dot{x_\phi} &= A_\phi*x_\phi + B_\phi*\phi^d\\
		\phi &= C*x_\phi\\
		
		\dot{x_\theta} &= A_\theta*x_\theta + B_\theta*\theta^d\\
		\theta &= C*x_\theta\\

		\dot{x}_{\dot\psi} &= A_{\dot\psi}*x_{\dot\psi} + B_{\dot\psi}*{\dot\psi}^d\\
		\dot\psi &= C*x_{\dot\psi}\\

	\end{cases}
\end{equation}

With the thrust curve we make a controller to generate frequency sweeps in roll and pitch 
and record the data for modeling the system response.  We use the Matlab 
System identification Toolbox to analyze the data and generate models form Roll, Pitch and Yaw as shown for Pitch with prediction 
of the model in figure \ref{fig:PitchModel}. The state-space system model generated Eq. \ref{eq:attitude} has the parameters show in the Table \ref{tab:state_space_values}.

 \begin{figure}[h!]
		\centering
		\includegraphics[width=1\textwidth]		{sysid_pitch.png}
		\caption{Pitch model}
		\label{fig:PitchModel}
			
\end{figure}

\begin{table}[h!]
\centering
	\begin{tabular}{| r | l |}
		\hline
		Roll 	&  \\ 
			 
		A  	&	$\begin{bmatrix}
				0.9972714286	& 0.0480328571	 \\ 
				-0.0426285714	& 0.8490285714 
				\end{bmatrix}$ \\

			&   \\		  
		B	&	$\begin{bmatrix}0.0001486 \\
				-0.0009289857	\end{bmatrix}$\\
			& \\		  
		C	&	$\begin{bmatrix}1.6295		& 0.0175517143 \end{bmatrix}$ \\
		\hline
		Pitch & \\ 
		
		A  	&	$\begin{bmatrix}
				0.9964625	& 0.04518875	 \\ 
				-0.02942875	& 0.8601625 
				\end{bmatrix}$ \\

			&   \\		  
		B	&	$\begin{bmatrix}0.0002542 \\
				-0.0013327	\end{bmatrix}$\\
			& \\		  
		C	&	$\begin{bmatrix}2.57185		& 0.03042025 \end{bmatrix}$ \\
		 	 	
		\hline
		Yaw &  \\ 
		A  	&	$\begin{bmatrix} 0.9571982 \end{bmatrix}$ \\
			&   \\		  
		B	&	$\begin{bmatrix}0.0002470240 \end{bmatrix}$\\
			& \\		  
		C	&	$\begin{bmatrix}2.379545 \end{bmatrix}$ \\
		
		\hline
	\end{tabular}
	\caption{Attitude Model Values for the state-space system}
\label{tab:state_space_values}
\end{table}


\section{Velocity Control}
\label{section:velocity_control}
The velocity controller receives an x, y, z velocity command and issues roll, pitch and thrust commands to 
try to achieve the desired velocity.  The velocity controller is also responsible for controlling the vehicle's heading.  
To do this, it receives a heading command and issues a heading rate to the vehicle.  On a multi-rotor vehicle the 
heading can be decoupled from the translational dynamics, so it will be controlled independently.

The horizontal velocity is controlled by varying the roll and pitch of the vehicle to direct the thrust 
vector of the vehicle in the desired direction of travel.  The vertical velocity is controlled by 
varying the total downward thrust of the vehicle.

This work presents a control system based on PID controllers.  PID is selected primarily because of 
its simplicity and robustness. We have use separate PID parameter set for roll and pitch as their
model in system identification was different as mounting of sensors affected the moment of inertia. 
We found that state estimation is too noisy to allow any good differential term in PID control and 
later we choose to apply low-pass filter on the input odometry and achieved much smoother control. 
The thrust control also calculates the force need to overcome gravity using the parameters found 
in section \ref{section:sysid} in the Equation \ref{eq:thrustPID}.

 \begin{equation}
 \label{eq:thrustPID}
 \begin{split}  
	  F_{gravity} = gravity * mass  -b * 1/m\\
	  F_{thrust} = 	F_{gravity}  + \\
			K_p * error(t)  + \\
			K_i * \int^t_0 e(t) dt  + \\
			K_d * \frac{d(e(t))}{dt}
 \end{split} 
 \end{equation} 
\section{Trajectory Control}

After successfully stabilizing the velocity of the vehicle, it is possible to implement a trajectory 
controller.  This controller will receive trajectories from the planner and issue velocity commands 
to the velocity controller in order to track the trajectory.  The cross-track error is defined as the 
distance from the current position to the closest point along the path and the derivative of this 
error is the magnitude of the velocity in the same direction.  The along-track velocity error is 
defined as the the difference between the velocity encoded in the path and the velocity of the 
vehicle tangent to the path. The cross-track velocity command consists of a PID controller wrapped 
around the cross-track position error.  The along-track velocity command consists of a PI controller 
wrapped around the along-track velocity error.  The total velocity command applied to the vehicle 
is the sum of the along-track and cross-track velocity commands.

