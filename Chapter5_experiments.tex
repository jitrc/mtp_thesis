\chapter{Experiments}
  \label{chap:experiments}
  \minitoc
    \section{Hardware}
    This section describes the hardware and its boundaries to give basic information before the results are presented in the following section.
    The experiments were carried out in High-Bay and corridors of Newell Simon Hall and Gates Hillman Center at Robotics Institute, Carnegie Mellon University.
    \subsection{Vehicle}
      We used a four-propeller rotorcraft (quadrotor) which is the QuadroXL model of Mikrokopter sold by HiSystems GmbH \footnote{h!tp://www.mikrokopter.de}. This quadrotor already comes with a low level Flight Control which allows remote control as well as an additional external control. The external control is used for controlling the Mikrokopter with the mounted computer. Without any payload the weight of the Mikrokopter is 980 g. After mounting the battery, the computer and the sensors the overall weight is 2.14 kg. The maximum payload for the vehicle is around 1 kg depending on which kind of propellers are used.
      This vehicle has one 6200 mAh 4-cell lithium polymer batteries on board and provide the vehicle with about 10 minute flight time while carrying a 1.0 kg payload. The vehicle is controlled via a bi-directional serial port.  The main computer sends commands at 25 Hz to the vehicle that contain the desired roll angle, pitch angle, yaw rate and total thrust.  The vehicle sends telemetry data back to the main computer at 10 Hz which includes the vehicle's current attitude, motor velocities, and battery voltage.
      \begin{figure}[!h]
		\begin{center}
		\includegraphics[width=0.9\textwidth]{mk.JPG}
		\caption{ The Mikrokopter  }
		\label{fig:mk}
		\end{center}		
	\end{figure}

     \subsection{Sensor Payload}

      There are two \textbf{sensors} (figure \ref{fig:sensors}) mounted on the Mikrokopter: One is a forward facing \href{http://www.asus.com/Multimedia/Xtion\_PRO/\#specifications}{Asus Xtion Pro} Camera\footnote{http://www.asus.com/Multimedia/Xtion\_PRO/\#specifications} which is also slightly tilted downwards. It provides \ac{RGB-D} images at maximum frequency of 30 Hz and a VGA resolution with $640*480$ pixel. Therefore it comes with one RGB sensor and two depth - infrared sensors (transmitter and receiver). The reason for the downwards tilt is the limited range of the depth sensor which has a recommended range of 0.8 m to 3.5 m.
      The second INS sensor is the  microstrain\_3d\_gx\_35 \ac{IMU}. The \href{http://www.microstrain.com/datasheet/inertial-3}{microstrain\_3d\_gx\_35}\footnote{http://www.microstrain.com/datasheet/inertial-3} provides angular velocity and linear acceleration measurements.The INS has an accelerometer, gyroscope, magnetometer, and L1 band GPS receiver.  The GPS was not used during any of these experiments.  The inertial sensors are all MEMS devices and are relatively low performance.  The IMU measurements from this device are available at 50Hz.
      This entire assembly is mounted on vibration dampers to prevent the rotor vibration from being transferred to the INS.
	
	\begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{sensors.JPG}
		\caption[The Sensors \& Hardware]{ The Sensors: Asus Xtion Pro and microstrain\_3d\_gx\_35 \ac{IMU} (mounted on the camera). The computer is mounted directly on the Mikrokopter above the sensors.}
		\label{fig:sensors}
		\end{center}		
	\end{figure}

     \subsection{Computing payload}
      The main computer on board the vehicle has a Core 2 Duo dual-core processor running at 1.86GHz with 4GB of RAM.  This computer executes all of the high-level algorithms and interfaces with all of the sensors on board the vehicle.  
      For monitoring experiment progress we connect wirelessly via 802.11n. The \acs{CPU} board is connected to a \href{https://pixhawk.ethz.ch/electronics/base\_large}{PIXHAWK COM Express} carrier board \footnote{Similar to this board: https://pixhawk.ethz.ch/electronics/base\_large} which has Solid State Disk installed and provides following interfaces among others:
 
      \begin{itemize}
	      \item VGA port
	      \item USB (Type A)
	      \item RJ-45 Ethernet
	      \item 4x USB on a 4-pin PicoBlade
	      \item 3x UART on 4-pin PicoBlade 
	      \item CPU cooling fan power
	      \item ...
      \end{itemize}
 


     \section{Software }

      The \textbf{computer} have Ubuntu 12.04 LTS as an operating system. For autonomous control algorithms we use C++. 
      We use \acf{ROS} for its multi-process framework and visualization.  We also use ros to log both sensor data 
      and processed outputs for later analysis and improvement.  

      \href{http://www.willowgarage.com/pages/software/ros-platform} {ROS} \footnote{http://www.willowgarage.com/pages/software/ros-platform}
       provides libraries and tools to help software developers to create robot applications. 
       It provides hardware abstraction, device drivers, libraries, visualizers, message-passing, package management, and more. 
       For message-passing \ac{ROS} operates as an \ac{MOM} which provides a loose coupling between different clients by 
       using a publisher-subscriber software pattern (figure \ref{fig:PubSub})\cite[p. 5]{Curry2004}.

      \begin{figure} [h!]
	      \begin{center}
	      \includegraphics[width=0.9\textwidth]{PubSub.jpeg}
	      \caption[Publisher Subscriber Software Pattern]{The Publisher Subscriber Software Pattern is used in \ac{ROS} for distributing messages \cite[p. 10]{Curry2004} }
	      \label{fig:PubSub}
	      \end{center}		
      \end{figure}
		
      This pattern uses \ac{FIFO} message queues, also named topics, which support an asynchronous and anonymous 
      message exchange between multiple publishers and subscribers. These clients, which can be subscribers and
      publishers at the same time, need to publish messages to a specific topic to send message. On the other side 
      they can listen to a topic to receive messages. Waiting for messages is passive and enables subscribers to use 
      computing time while waiting for new messages. Each topic is identified by a name, a queue length and the type 
      of messages it manages. The queue size of every topic defines the size of the message buffer for the topic 
      and is necessary to enable the clients to work at different rates. Additionally the asynchronous management 
      of messages prevents the system to run into producer-consumer problems. This is done by decoupling the 
      performance characteristics of client. Nevertheless there is no guarantee that messages are read \cite[p. 6,7,9-11]{Curry2004}.
      
\subsection{Dynamic Simulation}
\label{section:dynamic_simulation}
 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.6\textwidth]		{line_tracking.png}
		\caption{Control Design in sim to follow line}
		\label{fig:Controlsim}
		\end{center}		
\end{figure}
In order to develop a trajectory tracking controller, it is first necessary to develop a dynamic model of the system to be controlled.  This is useful for determining an appropriate control approach and is also useful for testing the controller in simulation as shown in figure \ref{fig:Controlsim}.  By developing an accurate model of the vehicle, control algorithms can be tested in simulation before they are tested in flight.  This increases the ability to iterate the control design quickly without requiring time-consuming flight tests that may risk a crash. 

\begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{sim.png}
		\caption{Planner test using simulation}
		\label{fig:sim}
		\end{center}		
\end{figure}

We can also use the dynamic model fore testing planners as shown in figure \ref{fig:sim}




\section{Results}
  \label{sec:results}


With the control system from chapter \ref{chap:control}, 
and the vehicle described in chapter \ref{chap:experiments}, 
we have done some position velocity following and position hold hover tests.

 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=1\textwidth]		{positionholdxy.png}
		\caption{Hover XY drift plot in meter}
		\label{fig:hoverxy}
		\end{center}		
\end{figure}

Figure \ref{fig:hoverxy} shows the how much position in X and Y drifted while trying to hover and hold a single position. 
It stayed within a range of 0.5 meters, which is not very good but still stable to used a safe state while in autonomous mode. 




 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=1\textwidth]		{hav_c.png}
		\caption{Velocity controller with single roll and pitch PID}
		\label{fig:havc}
		\end{center}		
\end{figure}

 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{hav_d.png}
		\caption{Velocity controller with different roll and pitch PID}
		\label{fig:havd}
		\end{center}		
\end{figure}

Figure \ref{fig:havc} shows the how differently pitch and roll behaves 
by the magenta and green color in response to a velocity step command. 
After we improved the velocity controller with decoupled roll and pitch controller
figure \ref{fig:havd} show that we can now tune both roll and pitch to a lot similar and better control response.




Figure \ref{fig:disturbance} Shows the commanded position to hold and the position from state estimation. In this experiment
we manually move the robot to a different position by remote control while it was in autonomous position hold hover mode. The 
figure \ref{fig:disturbance} shows clearly that the robot came back to the same position and continued to hover there.
 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.9\textwidth]		{hold_disturbed.png}
		\caption{Position hold return returns to position after manual disturbance}
		\label{fig:disturbance}
		\end{center}		
\end{figure}

   \clearpage
 Figure \ref{fig:trajopt_resut} generation of collision free, smooth and dynamically feasible trajectory generation by the local planner.  
 \begin{figure}[h!]
		\begin{center}
		\includegraphics[width=1\textwidth]		{PlannedTrjecjtoryTryingtoMeetDynamicConstraints.png}
		\caption{Planned Trajectory meeting dynamic constraints}
		\label{fig:trajopt_resut}
		\end{center}		
\end{figure}

   \clearpage
 Figure \ref{fig:gplan} show the global planner trying to plan from starting room S to Goal room G. 
 The ellipse show the amount amount of predicted uncertainty at the different points of the plan.  
 The predicted uncertainty increases as the the plan progresses from one point to another but also decreases 
 due to information gain due to nearby wall  within the range of the RGB-D camera.
 Only the best paths of the Algorithm 2 found for each room are shown. In the final global plan the path 
 through room B has been chosen as the global plan which is optimum low cost (minimum distance covered) path that can reach the goal room.
 Observe that uncertainty at some point inside room B has gone very high, such that it would not be possible 
 to distinguish between the door of room D and room G from room B. But later it went to a corner of room B if improve it's 
 localization and reduce uncertainty and safely move on to G. This planner allowed  intermediate uncertainties 
 higher than the required uncertainty to reach it's goal unlike RRBT planner which tries to maintain a uncertainty limit throughout the plan.
 This planner also does not try to minimize the uncertainty like BRM but still reaches safely. Thus this planner optimizes the use of floor plan
 to get a perfect balance between path cost and uncertainty to reach it's goal with minimum cost possible.
  \begin{figure}[!hp]
   \begin{center}
    \includegraphics[width=0.9\textwidth]		{gplanner.png}
    \caption{Global planning with uncertainty}
    \label{fig:gplan}
  \end{center}		
\end{figure}